# Hello Docker

a very simple go app that runs in a scratch container.

The app does just output "Hello Docker".

With the following instructions you can:
* build the go application from sourcecode
* build a super minimal docker image
* run the docker image

```
go build -ldflags="-s -w" .
docker build . -t hello-docker:latest
docker run hello-docker:latest
```
